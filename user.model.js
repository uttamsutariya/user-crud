const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			trim: true,
			required: [true, "Name is required"],
		},
		email: {
			type: String,
			trim: true,
			unique: true,
			required: [true, "Email is required"],
		},
		contactNumber: {
			type: String,
			trim: true,
			required: [true, "Contact number is required"],
		},
		dob: {
			type: Date,
			required: [true, "User dob is required"],
		},
		age: {
			type: Number,
			required: true,
		},
		profilePicture: {
			type: String,
			trim: true,
			required: [true, "profile picture url is required"],
		},
		address: {
			type: String,
			trim: true,
			required: [true, "Name is required"],
		},
		gender: {
			type: String,
			enum: ["male", "female"],
			required: [true, "Gender is required"],
		},
	},
	{
		versionKey: false,
	}
);

module.exports = mongoose.model("User", userSchema);
