require("dotenv").config();
const { faker } = require("@faker-js/faker");

const mongoose = require("mongoose");
const User = require("./user.model");

const DB_URL = process.env.DB_URL;

mongoose.set({ strictQuery: true });
mongoose
	.connect(DB_URL)
	.then(async () => {
		console.log(`DB connected`);
		const n = parseInt(process.argv[2]);

		for (let i = 0; i < n; i++) {
			const user = generateRandomData();
			await User.create(user);
			console.log(`${i + 1} created`);
		}
	})
	.catch((error) => {
		console.log(error);
		process.exit(1);
	});

function getAgeFromDate(dateString) {
	var today = new Date();
	var birthDate = new Date(dateString);
	var age = today.getFullYear() - birthDate.getFullYear();
	var m = today.getMonth() - birthDate.getMonth();
	if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
		age--;
	}
	return age;
}

function generateRandomData() {
	const name = faker.name.firstName(); // Rowan Nikolaus
	const email = faker.internet.email(); // Kassandra.Haley@erich.biz
	const contactNumber = faker.phone.number("+91##########");
	const dob = faker.date.between("1995-01-01T00:00:00.000Z", "2010-01-01T00:00:00.000Z");
	const profilePicture = `https://api.multiavatar.com/${name}.png?apikey=sKfRNQJ5XVOnkW`;
	const address = `${faker.address.streetAddress()}, ${faker.address.cityName()}, ${faker.address.state()}`;
	const gender = faker.name.sex();

	return {
		name,
		email: email.toLowerCase(),
		contactNumber,
		dob,
		age: getAgeFromDate(dob),
		profilePicture,
		address,
		gender,
	};
}
