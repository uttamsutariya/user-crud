require("dotenv").config();

const express = require("express");

const app = express();

const PORT = process.env.PORT || 3030;
const DB_URL = process.env.DB_URL;

const mongoose = require("mongoose");
const User = require("./user.model");

mongoose.set({ strictQuery: true });
mongoose
	.connect(DB_URL)
	.then(() => {
		console.log(`DB connected`);
		app.listen(PORT, () => console.log(`server started at port: ${PORT}`));
	})
	.catch((error) => {
		console.log(error);
		process.exit(1);
	});

app.use(express.json());

// get user by id
app.get("/api/users/:id", async (req, res) => {
	const { id } = req.params;

	try {
		if (!id) {
			throw new Error("Provide id");
		}

		const user = await User.findById(id);

		if (!user) {
			throw new Error("User not found");
		}

		user._doc.dob = formateDate(user.dob);

		res.status(200).json({
			message: "User data fetched",
			data: {
				user,
			},
		});
	} catch (error) {
		console.log(error);
		res.status(400).json({
			error: error.message,
			data: [],
		});
	}
});

// update user by id
app.put("/api/users/:id", async (req, res) => {
	try {
		const { id } = req.params;

		const { name, email, dob, contactNumber, gender, address } = req.body;

		if (!id) {
			throw new Error("Please provide id");
		}

		if (!name || !email || !contactNumber || !dob || !gender || !address) {
			throw new Error("All fields are required");
		}

		const existUser = await User.countDocuments({ _id: id });

		if (!existUser) {
			throw new Error("User not found");
		}

		const userDataToUpdate = {
			name,
			email,
			gender,
			address,
			contactNumber,
			dob,
			age: getAgeFromDate(dob),
		};

		const updatedUser = await User.findByIdAndUpdate({ _id: id }, userDataToUpdate, { new: true });

		res.status(200).json({
			message: "User updated",
			data: {
				user: updatedUser,
			},
		});
	} catch (error) {
		console.log(error);
		res.status(400).json({
			error: error.message,
			data: [],
		});
	}
});

// delete user by id
app.delete("/api/users/:id", async (req, res) => {
	try {
		const { id } = req.params;

		if (!id) {
			throw new Error("Please provide id");
		}

		const existUser = await User.countDocuments({ _id: id });

		if (!existUser) {
			throw new Error("User not found");
		}

		await User.deleteOne({ _id: id });

		res.status(200).json({
			message: "User deleted",
		});
	} catch (error) {
		console.log(error);
		res.status(400).json({
			error: error.message,
			data: [],
		});
	}
});

// get all users
app.get("/api/users", async (req, res) => {
	try {
		const users = await User.find({}, { name: 1, email: 1, profilePicture: 1 }).sort({ name: 1 });

		res.status(200).json({
			message: "All users fetched",
			data: {
				users,
			},
		});
	} catch (error) {
		console.log(error);
		res.status(400).json({
			error: error.message,
			data: [],
		});
	}
});

// create new user
app.post("/api/users", async (req, res) => {
	const { name, email, dob, contactNumber, gender, address } = req.body;

	try {
		if (!name || !email || !contactNumber || !dob || !gender || !address) {
			throw new Error("All fields are required");
		}

		const existUser = await User.countDocuments({ email });

		if (existUser) {
			throw new Error("User already exist");
		}

		const profilePicture = `https://api.multiavatar.com/${name}.png?apikey=${process.env.MULTIAVATAR_APIKEY}`;

		const userData = {
			name,
			email,
			age: getAgeFromDate(dob),
			profilePicture,
			contactNumber,
			dob: new Date(dob),
			gender,
			address,
		};

		let newUser = await User.create(userData);

		res.status(201).json({
			message: "User created",
			data: {
				user: newUser,
			},
		});
	} catch (error) {
		console.log(error.message);
		res.status(400).json({
			error: error.message,
			data: [],
		});
	}
});

app.get("/api", (req, res) => {
	res.send(`<h2>This api allows all CRUD operations on <a href="/api/users">/api/users</a> endpoint</h2>`);
});

app.get("/", (req, res) => {
	res.send(`<h2>Go to /api endpoint to use this api<br><a href="/api/users">get all users</a></h2>`);
});

function formateDate(date) {
	const newDate = new Date(date);

	const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

	const day = newDate.getDate();
	const month = monthNames[newDate.getMonth()];
	const year = newDate.getFullYear();

	return `${day} ${month}, ${year}`;
}

function getAgeFromDate(dateString) {
	var today = new Date();
	var birthDate = new Date(dateString);
	var age = today.getFullYear() - birthDate.getFullYear();
	var m = today.getMonth() - birthDate.getMonth();
	if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
		age--;
	}
	return age;
}
